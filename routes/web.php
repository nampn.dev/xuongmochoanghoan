<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::resource('category','CategoryController');
Route::get('/admin','AdminController@index')->name('admin.admin');


Route::resource('product','ProductController');
Route::resource('about','AboutController');
Route::resource('banner','BannerController');
Route::resource('contact','ContactController');
Route::resource('tintuc','TintucController');
Route::resource('material','MaterialController');
Route::resource('extra','AdminController');
Route::resource('vendor','VendorController');
Route::resource('settings','SettingController');
Route::get('category/{cate}/delete', 'CategoryController@destroy');
Route::get('product/{pro}/delete', 'ProductController@destroy');
Route::get('material/{mater}/delete', 'MaterialController@destroy');
Route::get('about/{ab}/delete', 'AboutController@destroy');
Route::get('banner/{banner}/delete', 'BannerController@destroy');
Route::get('tintuc/{tintuc}/delete', 'TintucController@destroy');
Route::get('contact/{contact}/delete', 'ContactController@destroy');
Route::get('vendor/{vendor}/delete', 'VendorController@destroy');
Route::get('setting/{setting}/delete', 'SettingController@destroy');



Auth::routes();
Route::resource('/','HomeController');
Route::get('/layout', 'HomeController@index')->name('layout');
//Frontend
Route::get('/','FrontendController@index')->name('frontend');
Route::get('/gioithieu','FrontendController@introduce')->name('gioithieu.index');
Route::get('/sanpham','FrontendController@produce')->name('sanpham.index');
Route::get('/article','FrontendController@article')->name('article.index');
Route::get('/doitac','FrontendController@doitac')->name('doitac.index');



Route::get('/lienhe','FrontendController@lienhe')->name('lienhe.index');
Route::post('/lien-he', 'FrontendController@createContact')->name('taolienhe');
//chitiet
Route::get('/chi-tiet-san-pham/{slug}', 'FrontendController@chiTietSanPham')->name('ctsp');
Route::get('/chi-tiet-tin-tuc/{slug}', 'FrontendController@chitiettintuc')->name('cttt');
