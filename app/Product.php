<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];
    public function productImages(){
        return $this->hasMany('App\ProductImages', 'product_id', 'id');
    }
}
