<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::latest()->paginate(5);
        return view('admin.category.index',[
            'nam'=>$category,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('admin.category.create',[
            'cate' =>$category,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cate = new Category();
        $cate->name=$request->input('name');
        $cate->slug=Str::slug($request->input('name'));
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $filename = time().'_'.$file->getClientOriginalName();
            $path_upload = 'upload/category/';
            $request->file('image')->move($path_upload,$filename);
            $cate ->image=$path_upload.$filename;
        }
        $is_active=0;
        if ($request->has('is_active')){
            $is_active=$request->input('is_active');
        }
        $cate->is_active=$is_active;
        $cate->position = $request->input('position');
        $cate->save();
        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cate = Category::findorFail($id);
        return view('admin.category.edit',[
            'cate'=>$cate,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cate = Category::find($id);
        $cate->name=$request->input('name');
        $cate->slug = Str::slug($request->input('name'));
        $is_active=0;
        if ($request->has('is_active')){
            $is_active=$request->input('is_active');
        }
        $cate->is_active=$is_active;
        if ($request->hasFile('new_image')){
            //xóa ảnh
            @link(public_path($cate->image));
            //gọi ảnh
            $file = $request->file('new_image');
            //get tên
            $filename = time().'_'.$file->getClientOriginalName();
            //đường dẫn ảnh
            $path_upload = 'upload/category/';

            $request->file('new_image')->move($path_upload,$filename);
            $cate ->image=$path_upload.$filename;
        }
        $cate->save();
        return redirect()->route('category.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cate = Category::find($id);
        $cate->delete();
        return redirect('/category  ');
    }
}
