<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Banner::latest()->paginate(5);
        return view('admin.banner.index',[
            'banner' =>$banner,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banner = Banner::all();
        $cate =     Category::all();
        //dd($banner);
        return view('admin.banner.create',[
            'pro'=>$banner,
            'cate'=>$cate,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $banner = new  Banner();
        $banner->title=$request->input('title');
        $banner->slug=Str::slug($request->input('title'));
        $banner->content=$request->input('content');
        //upload anh
        if ($request->hasFile('image')){
            //get file
            $file = $request->file('image');
            //dat ten
            $filename = time().'_'.$file->getClientOriginalName();
            //upload thu muc
            $path_upload = 'upload/banner/';
            //thuc hien upload anh len
            $request->file('image')->move($path_upload,$filename);

            $banner->image=$path_upload. $filename;
        }

        $banner->save();
        return redirect()->route('banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::find($id);
        return view('admin.banner.edit',[
            'banner' =>$banner,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banner = Banner::find($id);
        $banner->title = $request->input('title');
        $banner->slug=Str::slug($request->input('title'));
        $banner->content = $request->input('content');
        if ($request->hasFile('new_image')){
            //xóa ảnh
            @link(public_path($banner->image));
            //gọi ảnh
            $file = $request->file('new_image');
            //get tên
            $filename = time().'_'.$file->getClientOriginalName();
            //đường dẫn ảnh
            $path_upload = 'upload/category/';

            $request->file('new_image')->move($path_upload,$filename);
            $banner ->image=$path_upload.$filename;
            $banner ->position = $request->input('position');

        }
        $banner->save();
        return redirect()->route('banner.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::find($id);
        $banner ->delete();
        return redirect('/banner');
    }
}
