<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $footer = Setting::latest()->paginate(5);
        return view('admin.settings.index',[
            'footer' => $footer,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $footer = Setting::all();
        return view('admin.settings.create',[
            'footer' =>$footer,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $footer = new Setting();
        $footer ->name = $request->input('name');
        $footer ->phone = $request->input('phone');
        $footer ->email = $request->input('email');
        $footer ->address = $request->input('address');
        $footer->save();
        return redirect()->route('settings.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $footer = Setting::find($id);
        return view('admin.settings.edit',[
            'footer' =>$footer,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $footer = Setting::find($id);
        $footer ->name = $request->input('name');
        $footer ->phone = $request->input('phone');
        $footer ->email = $request->input('email');
        $footer ->address = $request->input('address');
        $footer->save();
        return redirect()->route('settings.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $footer = Setting::find($id);
        $footer->delete();
        return redirect('/setting');
    }
}
