<?php

namespace App\Http\Controllers;



use App\About;
use Illuminate\Http\Request;
use Illuminate\Support\Str;




class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about = About::latest()->paginate(5);

        return view('admin.about.index',[
            'about'=>$about,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $about = About::all();
        return  view('admin.about.create',[
            'about' => $about,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $about = new About();
        $about->title = $request->input('title');
        $about->slug =Str::slug($request->input('title'));
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $filename = time().'_'.$file->getClientOriginalName();
            $path_upload = 'upload/category/';
            $request->file('image')->move($path_upload,$filename);
            $about ->image=$path_upload.$filename;
        }
        $about->content = $request->input('content');
        $about->position = $request->input('position');
        $is_active = 0;
        if ($request->has('is_active')){
            $is_active =$request->input('is_active');
        }
        $about->is_active=$is_active;

        $about->save();
        return redirect()->route('about.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = About::find($id);
        return view('admin.about.edit',[
            'about'=>$about,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $about = About::find($id);
        $about->title = $request->input('title');
        $about->slug=Str::slug($request->input('title'));
        if ($request->hasFile('new_image')){
            //xóa ảnh
            @link(public_path($about->image));//Khác với create ở đoạn này
            //gọi ảnh
            $file = $request->file('new_image');
            //get tên
            $filename = time().'_'.$file->getClientOriginalName();
            //đường dẫn ảnh
            $path_upload = 'upload/category/';

            $request->file('new_image')->move($path_upload,$filename);
            $about ->image=$path_upload.$filename;
        }
        $is_active = 0;
        if ($request->has('is_active')){
            $is_active =$request->input('is_active');
        }
        $about->is_active=$is_active;
        $about->content = $request->input('content');
        $about->position = $request->input('position');
        $about->save();
        return redirect()->route('about.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about=About::find($id);
        $about->delete();
        return redirect('/about');
    }
}
