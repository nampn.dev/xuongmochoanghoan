<?php

namespace App\Http\Controllers;

use App\About;
use App\Category;
use App\Contact;
use App\Product;
use App\Setting;
use App\Tintuc;
use App\Vendor;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $cate = Category::where('is_active', 1)->orderBy('position')->get();
        $hot = Product::where('is_active', 1)->where('is_hot', 1)->orderBy('position', 'DESC')->get();
        $about = About::where('is_active',1)->where('position', 3)->get();
        $tintuc = Tintuc::where('is_active',1)->limit(4)->orderBy('position')->get();
        $footer = Setting::where('id',1)->get();
        return view('frontend.index', [
            'crtPage' => 'home',
            'page'=>'home',
            'cate' => $cate,
            'hot' => $hot,
            'about'=>$about,
            'article' =>$tintuc,
            'footer' =>$footer,
            

        ]);
    }
   


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function introduce()
    {
        $footer = Setting::where('id',1)->get();
        $ab = About::where('is_active',1)->orderBy('position')->get();
        return view('frontend.gioithieu.index', [
            'crtPage' => 'gt',
            'page'=>'intro',
            'ab'=>$ab,
            'footer'=>$footer,
        ]);
    }
    public function produce()
    {
        
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            $product = Product::where('categories_id',$id)->get();
            $cate = Category::where('id',$id)->get();
            $footer = Setting::where('id',1)->get();
            return view('frontend.sanpham.index',[
                'crtPage' => 'sp',
                'page'=>'produce',
                'product' =>$product,
                'cate'=>$cate,
                'footer'=>$footer,
                ]);
        }else{
            $product = Product::all();
            $cate = '';
            $footer = Setting::where('id',1)->get();
            return view('frontend.sanpham.index',[
                'crtPage' => 'sp',
                'page'=>'produce',
                'product' =>$product,
                'cate'=>$cate,
                'footer'=>$footer,
                ]);
        }
    }
    public function article()
    {
        $footer = Setting::where('id',1)->get();
        $article = Tintuc::latest()->paginate(6);
        return view('frontend.article.index',[
            'crtPage' => 'tt',
            'article' =>$article,
            'page'=>'article',
            'footer' =>$footer,
        ]);
    }
    public function lienhe(){
        $footer = Setting::where('id',1)->get();
        $contact = Contact::all();
        return view('frontend.lienhe.index',[
            'crtPage' => 'lh',
            'page'=>'lienhe',
            'contact' =>$contact,
            'footer' =>$footer,

        ]);
    }
    public function doitac()
    
    {
        $footer = Setting::where('id',1)->get();
        $vendor = Vendor::where('is_active',1)->orderBy('position')->get();
        return view('frontend.doitac.index', [
            'crtPage' => 'dt',
            'page'=>'doitac',
            'vendor' =>$vendor,
            'footer'=>$footer,
        ]);
    }

    public function createContact(Request $request)
    {
        
        $contact = new Contact();
        $contact->name = $request->input('name');
        $contact->email = $request->input('email');
        $contact->phone = $request->input('phone');
        $contact->content = $request->input('content');
        $contact->save();
        return redirect('/');
    }
    public function chiTietSanPham($slug)
    {
        $footer = Setting::where('id',1)->get();
        $product = Product::where('slug', $slug)->first();
        $related_product = Product::where('categories_id', $product->categories_id)->paginate(4);
        $cate = Category::find($product->categories_id);
        $crtPage = 'sp';
        $page = 'produce';

        return view('Frontend.sanpham.chitiet', [
            'page' => $page,
            'crtPage' => $crtPage,
            'product' => $product,
            'category' => $cate,
            'related_product' => $related_product,
            'footer'=>$footer,
        ]);
    }
    public function chitiettintuc($slug){
        $footer = Setting::where('id',1)->get();
        $crtPage = 'tt';
        $page = 'article';
        $article = Tintuc::where('slug',$slug)->first();
        $tintuc = Tintuc::where('is_active',1)->limit(4)->orderBy('position')->get();
        return view('frontend.article.chitiettintuc',[
            'article'=>$article,
            'tintuc' => $tintuc,
            'page' => $page,
            'crtPage' => $crtPage,
            'footer' =>$footer,
        ]);
    }
   
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
