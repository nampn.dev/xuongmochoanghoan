<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::latest()->paginate(5);
        return view('admin.product.index',[
            'product'=>$product,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pro = Product::all();
        $cate =     Category::all();
        //dd($pro);
        return view('admin.product.create',[
            'pro'=>$pro,
            'cate'=>$cate,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pro = new  Product();
        $pro->name=$request->input('name');
        $pro->slug=Str::slug($request->input('name'));
        $pro->stock=$request->input('stock');
        $pro->cost=$request->input('cost');
        $pro->price=$request->input('price');
        $pro->sale=$request->input('sale');
        $is_active =0;$is_hot=0;
        if ($request->has('is_active')){
            $is_active =$request->input('is_active');
        }
        $pro->is_active = $is_active;
        if ($request->has('is_hot')){
            $is_hot =$request->input('is_hot');
        }
        $pro->is_hot = $is_hot;
        $pro->categories_id=$request->input('categories_id');
        //upload anh
        if ($request->hasFile('image')){
            //get file
            $file = $request->file('image');
            //dat ten
            $filename = time().'_'.$file->getClientOriginalName();
            //upload thu muc
            $path_upload = 'upload/product';
            //thuc hien upload anh len
            $request->file('image')->move($path_upload,$filename);

            $pro->image=$path_upload. $filename;
        }

      $pro->save();
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $cate = Category::all();
        return view('admin.product.edit',[
            'product' => $product,
            'cate' =>$cate,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pro = Product::find($id);
        $pro->name=$request->input('name');
        $pro->slug=Str::slug($request->input('name'));
        if ($request->hasFile('new_image')){
            @link(public_path($pro->image));
            //get file
            $file = $request->file('new_image');
            //dat ten
            $filename = time().'_'.$file->getClientOriginalName();
            //upload thu muc
            $path_upload = 'upload/product/';
            //thuc hien upload anh len
            $request->file('new_image')->move($path_upload,$filename);

            $pro->image=$path_upload. $filename;
        }

        $pro->stock=$request->input('stock');
        $pro->cost=$request->input('cost');
        $pro->price=$request->input('price');
        $pro->sale=$request->input('sale');
        $is_active =0;$is_hot=0;
        if ($request->has('is_active')){
            $is_active =$request->input('is_active');
        }
        $pro->is_active = $is_active;
        if ($request->has('is_hot')){
            $is_hot =$request->input('is_hot');
        }
        $pro->is_hot = $is_hot;
        $pro->categories_id=$request->input('categories_id');
        //upload anh

        $pro->save();
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect('/product  ');
    }
}
