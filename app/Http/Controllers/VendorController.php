<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendor = Vendor::latest()->paginate(5);

        return view('admin.vendors.index',[
            'vendor'=>$vendor
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendor = Vendor::all();
        return view('admin.vendors.create',[
            'vender'=>$vendor
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vendor= new Vendor();
        $vendor->name = $request->input('name');
        $vendor->slug = Str::slug($request->input('name'));
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $file_name = time() . '_' . $file->getClientOriginalName();
            $path_upload = 'upload/vendor/';
            $request->file('image')->move($path_upload,$file_name);
            $vendor->image = $path_upload.$file_name;
        }
        $vendor->content_title = $request->input('content_title');
        $vendor->content_description = $request->input('content_description');
        $vendor->save();
        return redirect()->route('vendor.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor = Vendor::find($id);
        return view('admin.vendors.edit',[
            'vendor'=>$vendor,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vendor= Vendor::find($id);
        $vendor->name = $request->input('name');
        $vendor->slug = Str::slug($request->input('name'));
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $file_name = time() . '_' . $file->getClientOriginalName();
            $path_upload = 'upload/vendor/';
            $request->file('image')->move($path_upload,$file_name);
            $vendor->image = $path_upload.$file_name;
        }
        $vendor->content_title = $request->input('content_title');
        $vendor->content_description = $request->input('content_description');
        $vendor->save();
        return redirect()->route('vendor.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor = Vendor::find($id);
        $vendor->delete();
        return redirect('/vendor');
    }
}
