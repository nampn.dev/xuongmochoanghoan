<?php

namespace App\Http\Controllers;

use App\Tintuc;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TintucController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tintuc = Tintuc::latest()->paginate(5);
        return view('admin.tintuc.index',[
            'tintuc' => $tintuc,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tintuc = Tintuc::all();
        return view('admin.tintuc.create',[
            'tintuc' =>$tintuc,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tintuc = new Tintuc();
        if ($request->hasFile('image')){
            //get file
            $file = $request->file('image');
            //dat ten
            $filename = time().'_'.$file->getClientOriginalName();
            //upload thu muc
            $path_upload = 'upload/tintuc/';
            //thuc hien upload anh len
            $request->file('image')->move($path_upload,$filename);

            $tintuc->image=$path_upload. $filename;
        }

        $tintuc ->title = $request->input('title');
        $tintuc ->slug = Str::slug($request->input('title'));
        $tintuc ->summary= $request->input('summary');
        $tintuc ->description = $request->input('description');
        $tintuc ->position = $request->input('position');


        $tintuc->save();
        return redirect()->route('tintuc.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tintuc = Tintuc::find($id);
        return view('admin.tintuc.edit',[
            'tintuc' =>$tintuc,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tintuc = Tintuc::find($id);
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $file_name = time(). '_'. $file->getClientOriginalName();
            $path_upload = 'upload/tintuc/';
            $request->file('image')->move($path_upload,$file_name);
            $tintuc ->image = $path_upload.$file_name;
        }
        $tintuc ->title = $request->input('title');
        $tintuc ->slug = Str::slug($request->input('title'));
        $tintuc ->summary= $request->input('summary');
        $tintuc ->description = $request->input('description');
        $tintuc ->position = $request->input('position');
        $tintuc->save();
        return redirect()->route('tintuc.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tintuc = Tintuc::find($id);
        $tintuc->delete();
        return redirect('/tintuc');
    }
}
