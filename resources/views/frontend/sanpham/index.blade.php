@extends('frontend.layout.main')
@section('content')
    <!-- CONTENT -->
<div class="product-wrap container">
    <div class="list-product">
        <div class="d-flex justify-content-between box-title">
            <h2 class="header-prod"> 
                @if ($cate == null)
                    Sản phẩm
                @else
                    {{$cate[0]->name}} 
                @endif
            </h2>
            <p class="see-all"> <a href="#"> Xem tất cả </a></p>
        </div>
        <div class="box-product">
            <div class="row">
                @foreach($product as $p)
                <div class="col-lg-3 col-sm-6">
                    <div class="product">
                        <div class="img">
                            <img src="{{$p->image}}" alt="Bàn uống nước" class="img-fluid">
                        </div>
                        <div class="info">
                            <p class="name"> <a href="{{ route('ctsp', ['slug'=>$p->slug]) }}"> {{$p->name}}</a>   </p>
                            <p class="vote">
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                            </p>
                            <p class="desc">{{$p->description}} </p>

                            <p class="price"> <span>{{$p->price  }}</span> VNĐ </p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
