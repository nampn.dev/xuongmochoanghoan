@extends('Frontend.layout.main')
@section('style')
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="/frontend/css/adminlte.min.css">
@endsection
@section('content')
    <!-- CONTENT -->
    <div class="container">
        <div class="breadcumb">
            <div class="d-flex align-items-center">
                <p style="text-transform: uppercase;"><a href="{{route('frontend')}}">Trang chủ </a></p>
                <span style="display: inline-block; margin-top:-15px; margin-left: 5px;margin-right: 5px;"> <i class="fas fa-angle-right"></i> </span>
                <p style="display: inline-block"><a href="">{{$category->name}}</a></p>
                <span style="display: inline-block; margin-top:-15px; margin-left: 5px;margin-right: 5px;"> <i class="fas fa-angle-right"></i> </span>
                <p style="text-transform: uppercase;"><a href="#"> Chi tiết sản phẩm </a></p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="product-detail">
            <h1> {{$product->name}} </h1>
            <div class="row mb-5 mt-3">
                <div class="col-lg-8">
                    <div class="detail-img">
                        <img src="{{asset($product->image)}}" class="product-image" alt="Product Image">
                    </div>
                    <div class="product-image-thumbs">
                        {{--  @foreach($product->product_Images as $img)
                            <div class="product-image-thumb"><img src="/{{$img->image}}" alt="Product Image">
                            </div>
                        @endforeach  --}}
                    </div>
                </div>
                <div style="padding-left: 65px;" class="col-lg-4">
                    <p  class="material"> Chất liệu </p>
                    {{--  <div class="d-flex material ">  --}}
                        {{--  @foreach($product->prd as $item)
                            <button class="active">
                                {{$item->name ?? 'Trống'}}
                            </button>
                        @endforeach  --}}
                    {{--  </div>  --}}
                    <div class="price">
                        <p class="old">{{number_format($product->price)}}VNĐ</p>
                        <p class="new">{{number_format($product->sale)}} VNĐ</p>
                    </div>
                    <div class="certify d-flex align-items-center">
                        <i class="fas fa-shield-alt mr-2">   Bảo hành sản phẩm lên đến 12 tháng</i>
                    </div>
                </div>
            </div>

            <div class="detail-info">
                <nav class="d-flex">
                    <div  class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-link active" id="nav-dt-tab" data-toggle="tab" href="#nav-dt" role="tab"
                           aria-controls="nav-dt" aria-selected="true">Đặc trưng</a>
                        <a class="nav-link" id="nav-ts-tab" data-toggle="tab" href="#nav-ts" role="tab"
                           aria-controls="nav-ts" aria-selected="false">Thông số</a>
                        <a class="nav-link" id="nav-bq-tab" data-toggle="tab" href="#nav-bq" role="tab"
                           aria-controls="nav-bq" aria-selected="false">Bảo quản</a>
                        <a class="nav-link" id="nav-bh-tab" data-toggle="tab" href="#nav-bh" role="tab"
                           aria-controls="nav-bht" aria-selected="false">Bảo hành</a>
                        <a class="nav-link" id="nav-ck-tab" data-toggle="tab" href="#nav-ck" role="tab"
                           aria-controls="nav-ck" aria-selected="false">Cam kết</a>
                    </div>
                </nav>
                <div  class="tab-content" id="nav-tabContent">
                    <ul>
                        <li class="tab-pane fade show active" id="nav-dt" role="tabpanel" aria-labelledby="nav-dt-tab">
                            {!! $product->featured !!}
                        </li>
                        {{--  <li class="tab-pane fade" id="nav-ts" role="tabpanel" aria-labelledby="nav-ts-tab">
                            {!! $product->specifications !!}
                        </li>
                        <li class="tab-pane fade" id="nav-bq" role="tabpanel" aria-labelledby="nav-bq-tab">
                            {!! $product->preservation !!}
                        </li>
                        <li class="tab-pane fade" id="nav-bh" role="tabpanel" aria-labelledby="nav-bh-tab">
                            {!! $product->guarantee !!}
                        </li>
                        <li class="tab-pane fade" id="nav-ck" role="tabpanel" aria-labelledby="nav-ck-tab">
                            {!! $product->commitment !!}
                        </li>  --}}
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="relate-product-wrapper container">
        <div  class="relate-product">
            <div class="d-flex justify-content-between header-re-prod">
                <h2> Sản phẩm tương tự </h2>
            </div>
            <div class="box-relate-product">
                <div class="row">
                    @foreach($related_product as $rl_prd)
                        @if($rl_prd->name!=$product->name)
                            <div class="col-lg-3 col-sm-6">
                                <div class="product">
                                    <div class="img">
                                        <a href="">
                                            <img src="{{asset($rl_prd->image)}}" alt="{{$rl_prd->name}}"
                                                 class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <p class="name">
                                            <a href=""
                                               title="{{$rl_prd->name}}">
                                                {{$rl_prd->name}}
                                            </a>
                                        </p>
                                        <p class="vote">
                                            <span><i class="fas fa-star"></i></span>
                                            <span><i class="fas fa-star"></i></span>
                                            <span><i class="fas fa-star"></i></span>
                                            <span><i class="fas fa-star"></i></span>
                                            <span><i class="fas fa-star"></i></span>
                                        </p>
                                        <p class="desc">{{$rl_prd->description}}</p>

                                        <p class="price"><span>{{number_format($rl_prd->price)}}</span> VNĐ </p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@section('detail_js')
    <!-- jQuery -->
    <script src="/frontend/js/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/frontend/js//bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/frontend/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/frontend/js/demo.js"></script>
@endsection

