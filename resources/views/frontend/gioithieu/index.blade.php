@extends('frontend.layout.main')
@section('content')
<!-- CONTENT -->
<div class="container">
    <div class="introduce">
        <div class="introduce-box container">
            @foreach($ab as $a)
                @if($a -> position == 1)
                <div>
            <h2 class="header-introduce"> {{$a->title}}</h2>
            <div class="row">
                <div class="col-lg-6">
                    <div class="img mb-4">
                        <img src="{{asset($a->image)}}"  class="img-fluid w-100">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="content text-justify">

                        <p>{!! $a->content !!}</p>

                    </div>
                </div>
            </div>
        </div>
@else
            <div>
        <div class="view-box px-0 px-md-4">
            <h2 class="header-view"> {{$a->title}} </h2>
            <div class="img">
                <img src="{{asset($a->image)}}" alt="tầm nhìn" class="w-100">
            </div>
            <div class="content text-justify mt-3">
                    <p>{!! $a->content !!}</p>
            </div>
        </div>
            </div>
@endif
        @endforeach
        <div class="duty-box">
            <h2 class="header-duty"> sứ mệnh </h2>
            <div class="row reason">
                <div class="col-lg-6">
                    <div class="reason-item d-flex">
                        <div class="img">
                            <img src="/frontend/images/AnhCat/voi-xa-hoi.png" alt="Với xã hội">
                        </div>
                        <div class="content">
                            <h3 class="title"> Với xã hội </h3>
                            <p class="desc">
                            </p><p>Hài hòa lợi ích doanh nghiệp với lợi ích xã hội, tích cực cùng cộng đồng xây dựng môi trường sống bền vững.</p>

                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="reason-item d-flex">
                        <div class="img">
                            <img src="/frontend/images/AnhCat/voi-nhan-vien.jpg" alt="Với nhân viên">
                        </div>
                        <div class="content">
                            <h3 class="title"> Với nhân viên </h3>
                            <p class="desc">
                            </p><p>Xây dựng môi trường làm việc chuyên nghiệp, năng động, sáng tạo và nhân văn.</p>

                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="reason-item d-flex">
                        <div class="img">
                            <img src="/frontend/images/AnhCat/voi-doi-tac.jpg" alt="Với đối tác">
                        </div>
                        <div class="content">
                            <h3 class="title"> Với đối tác </h3>
                            <p class="desc">
                            </p><p>Xây dựng môi trường làm việc chuyên nghiệp, năng động, sáng tạo và nhân văn.</p>

                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="reason-item d-flex">
                        <div class="img">
                            <img src="/frontend/images/AnhCat/voi-thi-truong.png" alt="Với thị trường">
                        </div>
                        <div class="content">
                            <h3 class="title"> Với thị trường </h3>
                            <p class="desc">
                            </p><p>Cung cấp các sản phẩm với chất lượng quốc tế và phù hợp với con người Việt Nam.</p>

                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
