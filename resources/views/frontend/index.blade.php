@extends('frontend.layout.main')
@section('content')
    <!-- //content -->
 <div class="content-wrap container">
    <div class="categories">
        @foreach($cate as $item)
        <div class="category-item">
            <a href="{{ route('sanpham.index',['id'=>$item->id]) }}">
                <div class="category-img">
                    <img src="{{asset($item->image)}}" alt="{{ $item ->name }}" class="img-cate">
                </div>
                <p>{!! $item->name !!} </p>
            </a>
        </div>
   @endforeach
</div>    
    </div>
<!-- HOT PRODUCT -->
<div class="hot-product-wrap container">
    <h2 class="header-prd"> Sản phẩm nổi bật </h2>
    <div class="slide-prd">
        @foreach($hot as $h)
            <div class="product">
                <div class="img">
                    <a href="">
                        <img src="{{asset($h->image)}}" alt="{{$h->name}}" class="img-fluid">
                    </a>
                </div>
                <div class="info">
                    <p class="name">
                        <a href="{{ route('ctsp', ['slug'=>$h->slug]) }}">{{$h->name}}</a>
                    </p>
                    <p class="vote">
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="fas fa-star"></i></span>
                    </p>
                    <p class="desc">{{$h->description}}</p>
                    <p class="price">
								<span>
									{{number_format($h->price)}}
								</span> VNĐ
                    </p>
                </div>
            </div>
        @endforeach
    </div>
</div>
<div class="about-us">
    <div class="container">
        <h2 class="header-abt"> Về chúng tôi </h2>
        <div class="row">
            @foreach($about as $ab)
            <div class="col-lg-6">
                <div class="img h-100">
                    <img src="{{asset($ab->image)}}" alt="NỘI THẤT HOÀNG HOAN UY TÍN SONG HÀNH CHẤT LƯỢNG" class="w-100 h-100" >
                </div>
            </div>
            <div class="col-lg-6">
                <div class="content h-100">
                    <h3> {{{$ab->title}}}  </h3>
                    <div>
                        <p>{!! $ab->content !!}</p>

                    </div>
                    <div>
                        <p>
                            <img alt="giới thiệu" src="/frontend/images/AnhCat/tintuc-1.png">
                            <img alt="giới thiệu" src="/frontend/images/AnhCat/tintuc-2.png">
                            <img alt="giới thiệu" src="/frontend/images/AnhCat/tintuc-3.png">
                            <a href="/gioithieu">
                                <img alt="" src="/frontend/images/AnhCat/vct-4.png">
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <br><br>
        <h2 class="header-abt"> Tại sao nên chọn hoàng hoan?</h2>
        <div class="row reason">
            <div class="col-md-6">
                <div class="reason-index-item d-flex">
                    <div class="img">
                        <img src="/frontend/images/AnhCat/money.png" alt="lý do 1">
                    </div>
                    <div class="content">
                        <h3 class="title"> Chính sách giá </h3>
                        <p class="desc"> Tốt nhất và công khai giá trên website</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="reason-index-item d-flex">
                    <div class="img">
                        <img src="/frontend/images/AnhCat/product.png" alt="lý do 1">
                    </div>
                    <div class="content">
                        <h3 class="title"> Sản xuất  </h3>
                        <p class="desc"> Trực tiếp sản xuất bởi đội ngũ nhân viên hàng đầu</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="reason-index-item d-flex">
                    <div class="img">
                        <img src="/frontend/images/AnhCat/medal.png" alt="lý do 1">
                    </div>
                    <div class="content">
                        <h3 class="title"> Chất lượng </h3>
                        <p class="desc"> Cam kết chất lượng sản phẩm và tốc độ thi công </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="reason-index-item d-flex">
                    <div class="img">
                        <img src="/frontend/images/AnhCat/open-24-h.png" alt="lý do 1">
                    </div>
                    <div class="content">
                        <h3 class="title"> Bảo hành  </h3>
                        <p class="desc"> Dịch vụ bảo hành tốt nhất khu vực</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- NEWS -->
<div class="news">
    <div class="container">
        <h2 class="header-news"> Tin tức </h2>
        <div class="row">
            <div class="col-lg-7">
                @foreach($article as $t)
                @if($t->position == 1)
                <div class="box">
                    <div class="img">
                        <img src="{{asset($t->image)}}" class="w-100">
                    </div>
                    <div class="news-content">
                        <p class="title">
                            <a href="{{ route('cttt',['slug'=>$t->slug]) }}">
                               {!! $t->title !!}
                            </a>
                        </p>
                        <div class="desc">
                                <p>{!! $t->description !!}</p>
                        </div>
                    </div>
                </div>
                    @endif
                @endforeach
            </div>

            <div class="col-lg-5 ">
                <ul>
                    @foreach($article as $t)
                        @if($t->position != 1)
                    <li class="d-flex">
                        <div class="img">
                            <img src="{{asset($t->image)}}" >
                        </div>
                        <div class="content">
                            <p class="title">
                                <a href="{{ route('cttt',['slug'=>$t->slug]) }}">
                                    {!! $t->title !!}
                                </a>
                            </p>
                            <div class="desc sub-news-content">{!! $t->description !!}</div>
                        </div>
                    </li>
                        @endif
                            @endforeach

                </ul>
                <div>
                    <a href="#" class="see-more"> Xem thêm  <i class="fas fa-long-arrow-alt-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- PARTNER -->
<div class="partner">
    <div class="container">
        <h2 class="header-ptn"> Đối tác </h2>

        <!-- Swiper -->
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide ptn-slide">
                    <div class="ptn-item">
                        <div class="img">
                            <img src="/frontend/images/AnhCat/vinpearl.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="ptn-item">
                        <div class="img">
                            <img src="/frontend/images/AnhCat/muong-thanh.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="ptn-item">
                        <div class="img">
                            <img src="/frontend/images/AnhCat/sheraton.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="ptn-item">
                        <div class="img">
                            <img src="/frontend/images/AnhCat/the-coffee-house.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="ptn-item">
                        <div class="img">
                            <img src="/frontend/images/AnhCat/marvella.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="ptn-item">
                        <div class="img">
                            <img src="/frontend/images/AnhCat/sunrise-sapa.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="ptn-item">
                        <div class="img">
                            <img src="/frontend/images/AnhCat/melissa.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CONTACT -->
<div class="contact contact-index">
    <span><img src="/frontend/images/AnhCat/ghe.png" alt="Trải nghiệm cùng hoàng hoan"></span>
    <div class="container">
        <div class="row contact-row">
            <div class="col-lg-6 col-md-5 ">
                <h2 class="title"> Trải nghiệm dịch vụ <br> <strong> cùng Hoàng hoan ngay </strong></h2>
            </div>
            <div class="col-lg-6 col-md-7">
                <p class="mb-1 text-white"> Thông tin liên hệ </p>
                <form method="post" action="{{route('lienhe.index')}}">
                    @csrf
                    <div class="form-group">
                        <input class="infor" type="text" placeholder="Email/Số điện thoại" name="homeContact">
                        <button class="savePhone"> Gửi</button>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
</div>
@endsection

