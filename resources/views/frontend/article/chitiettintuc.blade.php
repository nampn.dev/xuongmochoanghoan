@extends('Frontend.layout.main')
@section('content')
 <!-- CONTENT -->
 <div class="container">
    <div class="breadcumb">
        <div class="d-flex align-items-center">
            <p style="text-transform: uppercase;"><a href="{{route('frontend')}}">Trang chủ </a></p>
            <span style="display: inline-block; margin-top:-15px; margin-left: 5px;margin-right: 5px;"> </span>
            <p style="display: inline-block"><a href="">{{$article->name}}</a></p>
            <span style="display: inline-block; margin-left: 5px;margin-right: 5px;"> <i class="fas fa-angle-right"></i> </span>
            <p style="text-transform: uppercase;"><a href="#"> Chi tiết tin tức </a></p>
        </div>
    </div>
</div>
<section class="container">
    <div class="new-details">
        <div class="row">
            
            <div class="col-md-8 box-left">
                <h1 class="title"> {!! $article->title !!} </h1>
                <div>
                    <p>{!! $article->summary !!}</p>

                </div>
                <div>
                    <img alt="Độ tuổi khởi nghiệp và tự lập ngày càng trẻ hóa trong xã hội hiện đại thời nay, thế nên việc “thiết kế căn hộ chung cư mini 2 phòng ngủ”" src="{{ asset($article->image) }}" />

                    <p>{!! $article->description !!}</p>

                </div>
            </div>
            
            <div class="col-md-4 box-right">
                <h2> Bài viết mới  </h2>
                <ul class="list-unstyled">
                    @foreach ($tintuc as $tt)
                        <li class="d-flex">
                            <img src="{{ asset($tt->image) }}" alt="TUYỆT CHI&#202;U THIẾT KẾ CHUNG CƯ MINI 2 PH&#210;NG NGỦ SI&#202;U ĐẸP" class="img mr-2 mt-2" style="width:80px; height:80px;" />
                            <div class="w-100 mt-0">
                                <p class="mt-0 mb-1">
                                    <a href="{{route('cttt',['slug'=>$tt->slug])}}">{{ $tt->title }}</a>
                                </p>
                                <p class="d-flex justify-content-between" style="color:#999">
                                    <i class="fas fa-calendar-alt">&nbsp;  <span> 7/30/2020 11:54:35 PM </span></i>
                                    <i class="fas fa-eye">&nbsp;<span style="color:#f00">0</span></i>
                                </p>
                            </div>
                        </li>
                        @endforeach
                </ul> 
               
            </div>
           
        </div>
    </div>
</section>
@endsection