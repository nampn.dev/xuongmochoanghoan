<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <fieldset class="fieldset-job_location fieldset-type-text"> 
        <label for="job_location">Location 
            <small>(optional)</small>
        </label>
        <div class="field ">
           <input type="text" class="input-text" name="job_location" id="job_location" placeholder="e.g. &quot;London&quot;" value="" maxlength=""> 
           <small class="description">Leave this blank if the location is not important
           </small>
       </div>
   </fieldset>
   <fieldset class="fieldset-job_type fieldset-type-term-select">
    <label for="job_type">Job type</label>
    <div class="field required-field"> 
        <select name="job_type" id="job_type" class="postform">
            <option class="level-0" value="5">Freelance</option>
            <option class="level-0" value="2" selected="selected">Full Time</option>
            <option class="level-0" value="6">Internship</option>
            <option class="level-0" value="3">Part Time</option>
            <option class="level-0" value="4">Temporary</option>
             </select>
         </div>
     </fieldset>
</body>
</html>