{{--//Banner--}}
<div class="banner">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="/frontend/images/AnhCat/banner.png" class="d-block w-100" alt="...">
                <div class="content-box-banner">
                    <h2 class="text-uppercase header-banner">
                        THẾ GIỚI NỘI THẤT SỐ 1 VIỆT NAM <br> <span> Hoàng Hoan </span>
                    </h2>
                    <div class="sapo-banner">
                        <p>Sứ mệnh của chúng tôi là kết hợp hài hòa giữa ý tưởng và mong muốn của khách hàng, đem lại những phút giây thư giãn tuyệt vời bên gia đình và những người thân yêu.</p>
                    </div>
                    <a href="{{ route('lienhe.index') }}" class="text-uppercase btn-banner"> Liên hệ ngay </a>
                </div>
            </div>
            <div class="carousel-item">
                <img src="/frontend/images/AnhCat/banner.png" class="d-block w-100" alt="...">
                <div class="content-box-banner">
                    <h2 class="text-uppercase header-banner">
                        THẾ GIỚI NỘI THẤT SỐ 1 VIỆT NAM <br> <span> Hoàng Hoan </span>
                    </h2>
                    <div class="sapo-banner">
                        <p>Sứ mệnh của chúng tôi là kết hợp hài hòa giữa ý tưởng và mong muốn của khách hàng, đem lại những phút giây thư giãn tuyệt vời bên gia đình và những người thân yêu.</p>
                    </div>
                    <a href="{{ route('lienhe.index') }}" class="text-uppercase btn-banner"> Liên hệ ngay </a>
                </div>
            </div>
            <div class="carousel-item">
                <img src="/frontend/images/AnhCat/banner.png" class="d-block w-100" alt="...">
                <div class="content-box-banner">
                    <h2 class="text-uppercase header-banner">
                        THẾ GIỚI NỘI THẤT SỐ 1 VIỆT NAM <br> <span> Hoàng Hoan </span>
                    </h2>
                    <div class="sapo-banner">
                        <p>Sứ mệnh của chúng tôi là kết hợp hài hòa giữa ý tưởng và mong muốn của khách hàng, đem lại những phút giây thư giãn tuyệt vời bên gia đình và những người thân yêu.</p>
                    </div>
                    <a href="{{ route('lienhe.index') }}" class="text-uppercase btn-banner"> Liên hệ ngay </a>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
