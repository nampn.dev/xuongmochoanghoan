<!-- FOOTER -->
<footer class="w-100" style="background:#363636">
    <div class="container">
        <div class="row mt-3 mb-3">
            <div class="col-md-6">
                 @foreach ($footer as $ft)
                <h5>
                    THÔNG TIN CHUNG
                    <span class="line-remove" style="width: 78px; "></span>
                </h5>
               
                <h4 class="mt-2 pt-2 com-name">{{ $ft->name }}</h4>
                <p class="com-phone">
                    <i class="fas fa-phone-alt"></i>
                    <a href="#" title="0999.999.999">{{ $ft->phone }}</a>
                </p>
                <p class="com-email">
                    <i class="fas fa-envelope"></i>
                    <a href="#" title="cskh@hoanghoan.vn">{{ $ft->email }}</a>
                </p>
                <address class="com-address">
                    <i style="width: 22px;" class="fas fa-map-marker-alt"></i>{{ $ft->address }} </address>
                    @endforeach
            </div>
            <div class="col-md-3">
                <h5>
                    VỀ CHÚNG TÔI
                    <span class="line-remove" style="width: 78px; "></span>
                </h5>
                <ul>
                    <li><a href="{{ route('gioithieu.index') }}" title="Giới thiệu">Giới thiệu</a></li>
                    <li><a href="{{ route('sanpham.index') }}" title="Sản phẩm">Sản phẩm</a></li>
                    <li><a href="n{{ route('article.index') }}l" title="Tin tức">Tin tức</a></li>
                    <li><a href="{{ route('doitac.index') }}" title="Đối tác">Đối tác</a></li>
                    <li><a href="{{ route('lienhe.index') }}" title="Liên hệ">Liên hệ</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5>
                    KẾT NỐI VỚI CHÚNG TÔI
                    <span class="line-remove" style="width: 78px; "></span>
                </h5>
                <div class="mt-4 social-icon">
                    <a href="#" target="_blank">
                        <i class="fab fa-facebook-square"></i>
                    </a>
                    <a href="#" target="_blank">
                        <i class="fab fa-twitter-square"></i>
                    </a>
                    <a href="#" target="_blank">
                        <i class="far fa-envelope"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>
