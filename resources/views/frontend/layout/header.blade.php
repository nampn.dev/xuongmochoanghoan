{{--HEADER--}}
<div class="wrap">
    <header class="">
        <nav class="container">
            <div class="logo">
                <a href=""><img src="/frontend/images/AnhCat/logo.png" alt="Xưởng mộc giá tốt hoàng hoan"></a>
            </div>
            <div class="menu">
                <div class="d-flex h-100 justify-content-center align-items-center">
                    <ul>
                        <li class="{{$crtPage == 'home' ? 'active' : ''}}"><a href="/"> Trang chủ </a></li>
                        <li class="{{$crtPage == 'gt' ? 'active' : ''}}"><a href="{{route('gioithieu.index')}}"> Giới thiệu </a></li>
                        <li class="{{$crtPage == 'sp' ? 'active' : ''}}"><a href="{{route('sanpham.index')}}"> Sản phẩm </a></li>
                        <li class="{{$crtPage == 'tt' ? 'active' : ''}}"><a href="{{route('article.index')}}"> Tin tức </a></li>
                        <li class="{{$crtPage == 'dt' ? 'active' : ''}}"><a href="{{route('doitac.index')}}"> Đối tác  </a></li>
                        <li class="{{$crtPage == 'lh' ? 'active' : ''}}"><a href="{{route('lienhe.index')}}"> Liên hệ  </a></li>
                    </ul>
                </div>
            </div>
            <div class="d-flex align-items-center d-block d-lg-none">
                <button class="btn-menu-fixed d-block d-lg-none">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
        </nav>
    </header>
</div>
