@extends('frontend.layout.main')
@section('content')
<div class="container">
    <div class="contact-box">
        <div class="box px-0  px-md-4">
            <div class="cont-box d-flex">
                <div class="row">
                    <div class="col-lg-6 ">
                        <div class="img d-none d-lg-block">
                            <img src="/frontend/images/AnhCat/lien-he-img1.png" alt="Liên hệ" class="w-100">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="content">
                            <p class="title lien-he mb-2"> liên hệ với chúng tôi  </p>
                            <form method="post" action="{{route('taolienhe')}}">
                                @csrf
                            <div class="form-group lien-he">
                                <input type="text" name="name" placeholder="Họ và tên">
                                <input type="text" name="email" placeholder="Email">
                                <input type="text" name="phone" placeholder="Số điện thoại">
                                <input type="text" name="content" placeholder="Nội dung">
                                <br>
                                <button type="submit" class="contact-send"> Gửi </button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
