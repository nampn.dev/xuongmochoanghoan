@extends('admin.layout.main')
@section('content')
    <style>
        .card-danger .card-title {
            font-size: 1.5rem;
        }

        .card-danger .card-body {
            padding: .7em 1.25rem
        }
    </style>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12 d-flex justify-content-between align-items-center">
                    <h1>Sửa thông tin </h1>
                    <a href="/settings">
                        <button type="button" class="btn btn-info"><i class="fas fa-th-list mr-2"></i>Danh sách</button>
                    </a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content col-lg-12">
        <!-- left column -->
        <div class="col-md-12 col-lg-12">
            <!-- general form elements -->

            <div class="card">
            {{--                @if($errors->any())--}}
            {{--                    <div class="card-danger">--}}
            {{--                        <div class="card-header">--}}
            {{--                            <h3 class="card-title"><i class="fas fa-exclamation-triangle mr-2"></i>Lỗi!</h3>--}}
            {{--                            <div class="card-tools">--}}
            {{--                                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
            {{--                                    <i class="fas fa-times"></i>--}}
            {{--                                </button>--}}
            {{--                            </div>--}}
            {{--                            <!-- /.card-tools -->--}}
            {{--                        </div>--}}
            {{--                        <!-- /.card-header -->--}}
            {{--                        @foreach($errors->all() as $error)--}}
            {{--                            <div class="card-body bg-danger">{{$error}}</div>--}}
            {{--                        @endforeach--}}
            {{--                    </div>--}}
            {{--            @endif--}}
            <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{route('settings.update',['id'=>$footer->id])}}" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input value="{{$footer->name}}" type="text" class="form-control" id="name" name="name">
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Phone</label>
                            <input value="{{$footer->phone}}" type="number" class="form-control" id="phone" name="phone">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input value="{{$footer->email}}" type="text" class="form-control" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Address</label>
                            <input value="{{$footer->address}}" type="text" class="form-control" id="address" name="address">
                        </div>
                       

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Lưu</button>
                        <input type="reset" class="btn btn-default pull-right" value="Reset">
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>

        <!-- /.row -->
    </section>
@endsection

@section('js')
    <script type="text/javascript">
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    </script>
@endsection
