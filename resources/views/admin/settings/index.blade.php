@extends('admin.layout.main')
@section('content')
    <style>
        table tbody tr td:nth-child(4) p > img{
            width: 60px!important;
        }
    </style>
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h1>
                    <a href="{{route('settings.create')}}" class="btn btn-info add-new" style="float: right" >
                        <i class="fas fa-plus"></i> Thêm thông tin</a>

                </h1>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Address</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($footer as $key =>  $ft)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$ft->name}}</td>
                            <td>{{$ft->phone}}</td>
                            <td>{{$ft->email}}</td>
                            <td>{{$ft->address}}</td>




                            <td>


                                <a href="{{route('settings.edit',['id'=>$ft->id])}}" class="btn btn-info btn-sm">
                                    <i class="fas fa-pencil-alt"></i>
                                    Edit </a>
                                <a href="/settings/{{$ft->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Có muốn xóa không mà xóa?')">
                                    <i class="fas fa-trash"></i>
                                    Delete </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
                {{ $footer->links() }}
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

@endsection
