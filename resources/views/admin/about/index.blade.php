@extends('admin.layout.main')
@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3>
                    <a href="{{route('about.create')}}" class="btn btn-info" style="float:right">
                        <i class="fas fa-plus"></i> Thêm Thông Tin</a>
                </h3>


            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Content</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($about as $key =>  $ab)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$ab->title}}</td>
                            <td>
                                @if($ab->image)
                                    <img src="{{asset($ab->image)}}" alt="" width="64" height="64">
                                @endif
                            </td>
                            <td>{!!$ab->content!!}</td>

                            <td>

                                <a href="{{route('about.edit',['id'=>$ab->id])}}" class="btn btn-info btn-sm">
                                    <i class="fas fa-pencil-alt"></i>
                                    Edit
                                </a>
                                <a href="/about/{{$ab->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Có muốn xóa không mà xóa?')">
                                    <i class="fas fa-trash"></i>
                                    Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        {{ $about->links() }}
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    @endsection
