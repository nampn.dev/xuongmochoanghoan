@extends('admin.layout.main')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h1>
                    <a href="{{route('contact.create')}}" class="btn btn-info" style="float: right">
                        <i class="fas fa-plus">

                        </i> Thêm Thông Tin</a>
                </h1>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contact as $key =>  $con)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$con->name}}</td>
                            <td>{{$con->email}} </td>
                            <td>{{$con->phone}}</td>

                            <td>
                                <a href="{{route('contact.edit',['id'=>$con->id])}}" class="btn btn-info btn-sm">
                                    <i class="fas fa-pencil-alt"></i>
                                    Edit </a>
                                <a href="/contact/{{$con->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Có muốn xóa không mà xóa?')">
                                    <i class="fas fa-trash"></i>
                                    Delete </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        {{--        {{ $banner->links() }}--}}
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

@endsection
