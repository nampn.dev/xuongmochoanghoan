@extends('admin.layout.main')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h1>
                    <a href="{{route('material.create')}}" class="btn btn-info" style="float:right">
                        <i class="fas fa-plus"></i> Thêm Thông Tin</a>

                </h1>


            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($material as $key =>  $mater)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$mater->name}}</td>
                            <td>
                                <a class="btn btn-info btn-sm" href="{{route('material.edit',['id'=>$mater->id])}}">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                    Edit
                                </a>
                                <a class="btn btn-danger btn-sm" href="/material/{{$mater->id}}/delete" onclick="return confirm('Có muốn xóa không mà xóa?')">
                                    <i class="fas fa-trash">
                                    </i>
                                    Delete
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
                {{ $material->links() }}
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

@endsection
