@extends('admin.layout.main')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="col-sm-12">
                <h1>
                    <a href="{{route('product.create')}}" class="btn btn-info" style="float:right">
                        <i class="fas fa-plus" ></i> Thêm Thông Tin</a>
                </h1>
            </div>

            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Image</th>
                        <th>Stock</th>
                        <th>Cost</th>
                        <th>Price</th>
                        <th>Sale</th>

                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($product as $key =>  $pro)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$pro->name}}</td>
                            <td>{{$pro->slug }}</td>
                            <td>

                                @if($pro->image)
                                    <img src="{{asset($pro->image)}}" alt="" width="60" height="60">
                                @endif
                            </td>
                            <td>{{$pro->stock}}</td>
                            <td>{{$pro->cost}}</td>
                            <td>{{$pro->price}}</td>
                            <td>{{$pro->sale}}</td>
                            <td>{{$pro->description}}</td>

                            <td>


                                <a class="btn btn-info btn-sm"  href="{{route('product.edit',['id'=>$pro->id])}}" >
                                    <i class="fas fa-pencil-alt"></i>
                                    Edit
                                </a>
                                <a class="btn btn-danger btn-sm" href="/product/{{$pro->id}}/delete" onclick="return confirm('Có muốn xóa không mà xóa?')">
                                    <i class="fas fa-trash"></i>
                                    Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        {{ $product->links() }}
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

@endsection
