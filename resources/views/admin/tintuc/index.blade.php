@extends('admin.layout.main')
@section('content')
    <style>
        table tbody tr td:nth-child(4) p > img{
            width: 60px!important;
        }
    </style>
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h1>
                    <a href="{{route('tintuc.create')}}" class="btn btn-info add-new" style="float: right" >
                        <i class="fas fa-plus"></i> Thêm thông tin</a>

                </h1>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Summary</th>
                        <th>Description</th>
                        <th>Position</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tintuc as $key =>  $tt)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>
                                @if($tt->image)
                                    <img src="{{asset($tt->image)}}" alt="" width="60" height="60">
                                @endif
                            </td>
                            <td>{{$tt->title}}</td>
                            <td>{!! $tt->summary !!} </td>
                            <td>{{$tt->description}}</td>
                            <td>{{$tt->position}}</td>




                            <td>


                                <a href="{{route('tintuc.edit',['id'=>$tt->id])}}" class="btn btn-info btn-sm">
                                    <i class="fas fa-pencil-alt"></i>
                                    Edit </a>
                                <a href="/tintuc/{{$tt->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Có muốn xóa không mà xóa?')">
                                    <i class="fas fa-trash"></i>
                                    Delete </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
                {{ $tintuc->links() }}
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

@endsection
