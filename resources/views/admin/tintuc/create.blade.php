@extends('admin.layout.main')
@section('content')
    <style>
        .card-danger .card-title {
            font-size: 1.5rem;
        }

        .card-danger .card-body {
            padding: .7em 1.25rem
        }
    </style>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12 d-flex justify-content-between align-items-center">
                    <h1>Thêm Mới </h1>
                    <a href="/tintuc">
                        <button type="button" class="btn btn-info"><i class="fas fa-th-list mr-2"></i>Danh sách</button>
                    </a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content col-lg-12">
        <!-- left column -->
        <div class="col-md-12 col-lg-12">
            <!-- general form elements -->

            <div class="card">
            {{--                @if($errors->any())--}}
            {{--                    <div class="card-danger">--}}
            {{--                        <div class="card-header">--}}
            {{--                            <h3 class="card-title"><i class="fas fa-exclamation-triangle mr-2"></i>Lỗi!</h3>--}}
            {{--                            <div class="card-tools">--}}
            {{--                                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
            {{--                                    <i class="fas fa-times"></i>--}}
            {{--                                </button>--}}
            {{--                            </div>--}}
            {{--                            <!-- /.card-tools -->--}}
            {{--                        </div>--}}
            {{--                        <!-- /.card-header -->--}}
            {{--                        @foreach($errors->all() as $error)--}}
            {{--                            <div class="card-body bg-danger">{{$error}}</div>--}}
            {{--                        @endforeach--}}
            {{--                    </div>--}}
            {{--            @endif--}}
            <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{route('tintuc.store')}}" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="exampleInputFile">Ảnh phòng</label>
                                <input type="file" class="" id="image" name="image">
                            </div>
                            <label for="exampleInputEmail1">Tiêu đề </label>
                            <input value="{{old('name')}}" type="text" class="form-control" id="title" name="title">
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Nội dung</label>
                            <input value="{{old('name')}}" type="text" class="form-control" id="summary" name="summary">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mô Tả</label>
                            <input value="{{old('name')}}" type="text" class="form-control" id="description" name="description">
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Tạo</button>
                        <input type="reset" class="btn btn-default pull-right" value="Reset">
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>

        <!-- /.row -->
    </section>
@endsection

@section('js')
    <script type="text/javascript">
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    </script>
@endsection
