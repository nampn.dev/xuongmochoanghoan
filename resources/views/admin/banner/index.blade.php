@extends('admin.layout.main')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h1>
                    <a href="{{route('banner.create')}}" class="btn btn-info" style="float: right">
                        <i class="fas fa-plus">

                        </i> Thêm Thông Tin</a>

                </h1>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tiêu Đề</th>
                        <th>Nội Dung</th>
                        <th>Ảnh</th>
                        <th>Vị trí</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($banner as $key =>  $ban)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$ban->title}}</td>
                            <td>{!! $ban->content!!} </td>

                            <td>
                                @if($ban->image)
                                    <img src="{{asset($ban->image)}}" alt="" width="64" height="64">
                                @endif
                            </td>
                            <td>{{$ban->position}}</td>
                            <td>
                                <a href="{{route('banner.edit',['id'=>$ban->id])}}" class="btn btn-info btn-sm">
                                    <i class="fas fa-pencil-alt"></i>
                                    Edit </a>
                                <a href="/banner/{{$ban->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Có muốn xóa không mà xóa?')">
                                    <i class="fas fa-trash"></i>
                                    Delete </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        {{ $banner->links() }}
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

@endsection
