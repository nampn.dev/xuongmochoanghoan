@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>
                        <a href="{{route('category.create')}}" class="btn btn-info"  style="float:right">
                            <i class="fas fa-plus"></i> Thêm Thông Tin </a>
                    </h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <div class="row">
        <div class="col-12">
            <div class="card">

                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>User</th>

                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($nam as $key =>  $cate)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$cate->name}}</td>
                            <td>
                                @if($cate->image)
                                    <img src="{{asset($cate->image)}}" alt="" width="64" height="64">
                                    @endif
                            </td>
                            <td><span class="tag tag-success">Admin</span></td>
                            <td>

                                 <a class="btn btn-info btn-sm" href="{{route('category.edit',['id'=>$cate->id])}}">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Edit
                                    </a>
                               <a class="btn btn-danger btn-sm" href="/category/{{ $cate->id }}/delete" onclick="return confirm('Có muốn xóa không mà xóa?')">
                                        <i class="fas fa-trash">
                                        </i>
                                        Delete
                                    </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            {{ $nam->links() }}
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
