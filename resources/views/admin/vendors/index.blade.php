@extends('admin.layout.main')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="col-sm-12">
                    <h1>
                        <a href="{{route('vendor.create')}}" class="btn btn-info" style="float:right">
                            <i class="fas fa-plus" ></i> Thêm Thông Tin</a>
                    </h1>
                </div>

            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>Image</th>
                        <th>Content_Title</th>
                        <th>Content_Description</th>
                        <th>Position</th>
                        <th>Hoạt Động</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($vendor as $key =>  $ven)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$ven->name}}</td>
                            <td>{{$ven->slug }}</td>
                            <td>

                                @if($ven->image)
                                    <img src="{{asset($ven->image)}}" alt="" >
                                @endif
                            </td>
                            <td>{{$ven->content_title}}</td>
                            <td>{{$ven->content_description}}</td>
                            <td>{{$ven->positon}}</td>
                            <td>
                                <a class="btn btn-info btn-sm"  href="{{route('vendor.edit',['id'=>$ven->id])}}" >
                                    <i class="fas fa-pencil-alt"></i>
                                    Edit
                                </a>
                                <a class="btn btn-danger btn-sm" href="/vendor/{{$ven->id}}/delete" onclick="return confirm('Có muốn xóa không mà xóa?')">
                                    <i class="fas fa-trash"></i>
                                    Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>

        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

@endsection
